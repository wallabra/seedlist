# Welcome to Seedlist.

With Seedlist, you can _volatily_ encrypt data into a code and a secret
key, and then decrypt it back using the same code and key.

    >>> import seedlist
    >>> sl = seedlist.Seedlist()
    >>> enc, key = sl.encrypt(b'01234')
    >>> sl.decrypt(enc, key)
    b'01234'
    >>> enc
    b'%\x8e\x9d\xf8\xd6'
    >>> key
    bytearray(b'f\x15\xcc\xd9\x06c\xe2\xcd')
    >>>
    
This algorithm uses a PRNG created specifically for this purpose, for
reasons (although you can contribute with a better PRNG if so you
wish). More specifically, we want to be able to store two PRNG states,
that we can use later for generating these encryptions.

The initial seeds and seed deltas are usually generated as a _XOR_
operation between `random.randint(0, 0xFFFF)` and
`int(time.time()) & 0xFFFF`.