import itertools
    

def bit_slice(x, start, stop=None, relative=True):
    if stop is None:
        stop = 16
        
    if not relative:
        stop -= start
        
    mask = (2 ** stop - 1) << start
    return (x & mask) >> start

# 16-bit PRNG.
def evolve(seed):
    new_bits = ~(~bit_slice(seed, 0, 2) ^ ~(bit_slice(seed, 6, 2) ^ bit_slice(seed, 9, 2)))
    
    return (new_bits << 14 | seed >> 2) & 0xFFFF

def prng_infinite(seed):
    while True:
        seed = evolve(seed)
        yield seed
        
def prng(seed, amount=30):
    return itertools.islice(prng_infinite(seed), amount)

if __name__ == "__main__":
    import sys, random
    
    avg_p = 0
    i = 0
    min_p = None
    max_p = None
    
    for initial_seed in range(0x10000):
        seed = initial_seed
        cur_p = 0
        
        while True:
            seed = evolve(seed)
            
            if seed == initial_seed:
                break
                
            cur_p += 1
            
        if i == 0:
            avg_p = cur_p
            
        else:
            avg_p = ((avg_p * i) + cur_p) / (i + 1)
            
        print(initial_seed, cur_p, avg_p)
        i += 1
        
        if min_p is None or cur_p < min_p:
            min_p = cur_p
            
        if max_p is None or cur_p > max_p:
            max_p = cur_p
    
    print("Average PRNG period: {} | Min: {} | Max: {}".format(avg_p, min_p, max_p))