# == optional delta generation ==
import random
import time

# == struct byte packing ==
import struct

# == double seed sequence PRNG ==
from seedlist import sprng


class Seedlist(object):
    def __init__(self, a=None, b=None, delta_a=None, delta_b=None):
        t = int(time.time()) & 0xFFFF
        
        a = a or (random.randint(0, 0xFFFF) ^ t)
        b = b or (random.randint(0, 0xFFFF) ^ t)
        self.delta_a = delta_a or (random.randint(0, 0xFFFF) ^ t)
        self.delta_b = delta_b or (random.randint(0, 0xFFFF) ^ t)
        
        self.rng_a = sprng.generate(a, self.delta_a)
        self.rng_b = sprng.generate(b, self.delta_b)
        
    def encrypt(self, data):
        while len(data) < 2:
            data += b'\x00'
        
        key = []
        
        for i in range(len(data) - 1):
            ckey = (next(self.rng_a)[0] ^ next(self.rng_b)[0]) & 0xFFFF
            key.append(ckey)
            dkey = struct.unpack('H', data[i:i + 2])[0]
            seg = struct.pack('H', dkey ^ ckey)
            data = data[:i] + seg + data[i + 2:]
            # print(i, 'c=' + hex(ckey), 'd=0x' + seg.hex())
            
        bkey = bytearray()
        
        for k in key:
            bkey.append((k & 0xFF00) >> 8)
            bkey.append(k & 0xFF)
            
        return data, bkey
    
    def decrypt(self, data, bkey):
        key = []
        
        for i in range(int(len(bkey) / 2)):
            key.append(bkey[i * 2] << 8 | bkey[i * 2 + 1])
        
        for i in range(len(data) - 2, -1, -1):
            dkey = struct.unpack('H', data[i:i + 2])[0]
            ckey = key[i]
            # print(i, 'c=' + hex(ckey), 'd=' + hex(dkey))
            data = data[:i] + struct.pack('H', dkey ^ ckey) + data[i + 2:]
            
        return data